# README #

## 3 Files in this repo ##
- ctihtml:
A html page with cti view inside. The codes inside are JS, JQuery that integrated with socket.
This JQuery is listening the socket for any changes of data.
- socketcti:
A nodeJs server with socket inside. Running in port 3000 with localhost
- socketctidemo:
A html page that has a very simple form for sending a dummy data to the socket. Built for testing purpose.

Makesure the server (socketcti) is running before the ctihtml and socketctidemo be opened.
The socket is hardcoded in localhost:3000

### Done ###
- ctihtml is integrated with socket
- Update any changes of data: There are field IDs, status/anything can be updated when the field ID is same.

### On Progress ###
- Integrated live environment
- Animation
- Sound