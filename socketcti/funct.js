module.exports = {
    initJson: function () {
        var json = {};
        var date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();

        json['date'] = date+'-'+month+'-'+year;
    
        var unanswered = {
            "status":false,
            "from":"REZA RAHARDIAN",
            "contact": "+6287825414348",
            "to":["Didi", "Lintang"],
            "time": "13:35"
        };
    
        var callHistory = [];
    
        json['unanswered'] = unanswered;
        json['calls'] = callHistory;
    
        return json;
    },

    historyHand: function(oldJson, newJson){

        // if(oldJson.unanswered.id == newJson.field_id && newJson.status != 'Unanswered'){
        //     var unanswered = {
        //         "status":false,
        //         "from":"",
        //         "contact": "",
        //         "to":[],
        //         "time": ""
        //     };
        //     oldJson['unanswered'] = unanswered;
        // }
        if(oldJson.unanswered.status){
            var unanswered = {
                "status":false,
                "from":"",
                "contact": "",
                "to":[],
                "time": ""
            };
            oldJson['unanswered'] = unanswered;
        }


        var isIdExist = false;
        var totalHistory = oldJson.calls.length;
        var i = 0;
        console.log('Total Lenght'+totalHistory);
        while(i < totalHistory){ //Kalau FIELD_ID Sudah ada, udpate field yang sudah ada

            console.log(oldJson.calls[i].id+'=='+newJson.id);
            if(oldJson.calls[i].id == newJson.id){
                oldJson.calls[i].status = newJson.status;
                oldJson.calls[i].from = newJson.from;
                oldJson.calls[i].to = newJson.to;
                oldJson.calls[i].contact = '+'+newJson.contact;
                isIdExist = true;
            }
            i++;
        }

        if(!isIdExist){ //Kalau FIELD_ID Belum ada, tambah field baru
            var newData = {
                "status": newJson.status,
                "time":newJson.time,
                "from":newJson.from,
                "to":newJson.to,
                "contact": '+'+newJson.contact,
                "id":newJson.id
            };
            oldJson.calls.unshift(newData);
        }

        if(oldJson.calls.length > 20){
            oldJson.calls.pop();
        }
        return oldJson;

    },

    deleteDouble: function(str){
        let revised = '';
        let parts = str.split(',');

        if (!str.includes(',')){
            return str;
        }
        
        if(parts[0].replace(/\s+/g, '') ==  parts[1].replace(/\s+/g, '')){
            revised = parts[0];
        }else{
            revised = str.substring(0, str.length - 2);
        }
        return revised;
    }

  };