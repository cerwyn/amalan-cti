import express from 'express';
import { stringify } from 'querystring';

const log = require('simple-node-logger').createSimpleLogger('project.log');
const dotenv = require('dotenv');
dotenv.config();
const fs = require('fs');
var cron = require('cron-scheduler');
const func = require('./funct.js');
const app = express();

const options = {
    cert: fs.readFileSync('/etc/letsencrypt/live/amalan.id-0001/fullchain.pem'),
    key : fs.readFileSync('/etc/letsencrypt/live/amalan.id-0001/privkey.pem')
}

const https = require('https').createServer(options, app);
const server = require('http').createServer(app);
const bodyParser = require('body-parser');

const io = require('socket.io').listen(https);
const PORT = process.env.PORT;

// config for production
https.listen(PORT);

//server.listen(PORT);
console.log('Server is Running on PORT'+PORT);

const connections = [];

var json = func.initJson();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

cron({
    timezone: 'Asia/Jakarta',
    on: '30 1 * * *',
    name: 'Delete Json'
  } , function () {
      json = func.initJson();
    log.info('JSON Refreshed at 1:30:', new Date().toDateString());
    console.log('JSON Refreshed at 1:30');
  });


app.get('/getjson', (req,res)=>{

        // Website you wish to allow to connect https://amalan--incoming--c.cs57.visual.force.com
        //PROD https://amalan--c.ap8.visual.force.com
        res.setHeader('Access-Control-Allow-Origin', 'https://amalan--c.ap8.visual.force.com');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    
        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);
    res.json(json);
});

app.get('/deljson', (req,res)=>{
    json = func.initJson();
    res.json(json);
});

app.post('/data', (req,res)=>{
    log.info('DATA:', JSON.stringify(req.body),' AT:',new Date().toDateString());
    
    var data = req.body;
    if(!isNaN(data.from)){
        data.from = '+'+data.from.replace(/\s/g,'');
    }
    console.log(stringify(data));

    //REFORMATING DATE from 2019-10-25 09:08:06
    var parts = req.body.time.split(' ');

    if(parts.length == 1){
        data.time = '--';
    }else{
        var date = parts[1].split(':');
        data.time = date[0]+':'+date[1];
    }
    
    data.to = func.deleteDouble(data.to);
    
    if(req.body.status == "Unanswered" && req.body.to != "Nobody"){
        json = func.historyHand(json,data);
        
        //Update unanswered Logs
        var unanswered = {
            "status":true,
            "from":data.from,
            "contact": '+'+data.contact,
            "to": data.to,
            "time": data.time,
            "id" : data.id
        };

        json['unanswered'] = unanswered;

    }else if(req.body.to == "Nobody"){

    }else{
        json = func.historyHand(json, data);
    }

    if(req.body.to != "Nobody"){
        console.log('emitting');
        io.emit('emit data', json);
        log.info('EMIT:', json,' AT:',new Date().toDateString());
        res.json(json);
    }
    res.json(json);
});

io.sockets.on('connection', (socket)=>{
    connections.push(socket);
    console.log('$s sockets is connected', connections.length);
    

    socket.on('disconnect', () => {
        connections.splice(connections.indexOf(socket), 1);
     });

     socket.on('sending message', (message) => {
        console.log('Message is received :', message);
        io.sockets.emit('new message', {message: message});
     });

     socket.on('add data', (data)=>{
        console.log('Data is received', data);
        
        if(data.status == "Incoming"){
            //Update di Incoming Area & Call History
            var incomingData = {
                "status":true,
                "from":data.from,
                "contact": data.contact,
                "to":[data.to],
                "time": data.time,
                "id" : data.id
            };
            json['incoming'] = incomingData;

            json = func.historyHand(json,data);

        }else{
            json = func.historyHand(json, data);
        }
        io.sockets.emit('new data', {data: json});
     });
});