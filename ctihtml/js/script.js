    var json = { 
        "date" : "20 October 2019",
        "incoming":{
          "status":true,
          "from":"REZA RAHARDIAN",
          "contact": "+6287825414348",
          "to":["Didi", "Lintang", "Alvin"],
          "time": "13:35"
        },
        "calls" : [
          {
            "time" : "13:38",
      "status" : "Answered",
      "from":"Cerwyn Eliata",
      "to":"Didi",
      "contact": "+6287825414348"
          },
          {
            "time" : "12:38",
      "status" : "Answered",
      "from":"Surya Prakoso",
      "to":"Rachel",
      "contact": "+6287825414348"
          },
      {
            "time" : "11:38",
      "status" : "Unanswered",
      "from":"Astrid Erasari",
      "to":"Lintang",
      "contact": "+6287825414348"
          },
      {
            "time" : "10:38",
      "status" : "Rejected",
      "from":"Clarisa",
      "to": "Fanny",
      "contact": "+6287825414348"
          },
      {
            "time" : "09:38",
      "status" : "Rejected",
      "from":"Trivia",
      "to": "Consult",
      "contact": "+6287825414348"
          },
          {
            "time" : "09:38",
      "status" : "Answered",
      "from":"Annabele",
      "to": "Consult",
      "contact": "+6287825414348"
          },
          {
            "time" : "09:38",
      "status" : "Rejected",
      "from":"Dwiyan",
      "to": "Consult",
      "contact": "+6287825414348"
          },
          {
            "time" : "09:38",
      "status" : "Unanswered",
      "from":"Brian",
      "to": "Consult",
      "contact": "+6287825414348"
          }
        ]
      }

  jQuery(document).ready(function() {
        var socket = io.connect('http://localhost:3002');

        socket.on('new data', function(data){
            json = data.data;
            var e = document.getElementById("divItemTop"); 
            e.innerHTML = "";
            
            var eb = document.getElementById("tableHistoryBottom"); 
            eb.innerHTML = "";

            var rmTable = document.getElementById("incomingConsultant"); 
            rmTable.innerHTML = "";

            if(json.incoming.status){  
                IncomingCallExist();
                UpdateCallHistory();
              }else{
                IncomingCallNotExist();
                UpdateCallHistory();
              }
        });
  });

//Initiate all variables
//Variables used in Incoming Number
var incomingCall = "";
var incomingName = "";
var incomingNumber = "";
var incomingConsultants = [];
var tableIncoming = document.getElementById("incomingConsultant"); 

//Variables used in Call History
var divItemTop = document.getElementById("divItemTop");
var tableHistoryBottom = document.getElementById("tableHistoryBottom");
document.getElementById("date").innerHTML = json.date;

if(json.incoming.status){  
  IncomingCallExist();
  UpdateCallHistory();
}else{
  IncomingCallNotExist();
  UpdateCallHistory();
}

function IncomingCallExist(){
  incomingName = json.incoming.from;
  incomingNumber = json.incoming.contact;
  document.getElementById("incomingName").innerHTML = '<b>'+incomingName+'</b>';
  document.getElementById("incomingNumber").innerHTML = incomingNumber;

  var totalConsultant = json.incoming.to.length;
  var i = 0;
  var row = 0;
  while(i < totalConsultant){
    var newRow = tableIncoming.insertRow(row);
    var newCell1 = newRow.insertCell(0);
    var newCell2 = newRow.insertCell(1);
    var fontStyle1 = '<font size="5"><b>';
    var fontStyle2 = '</b></font>';

    newCell1.innerHTML = fontStyle1+json.incoming.to[i]+fontStyle2;

    if (json.incoming.to[i+1]==undefined){
        newCell2.innerHTML = '';
    }else{
        newCell2.innerHTML = fontStyle1+json.incoming.to[i+1]+fontStyle2;
    }
    row++;
    i=i+2;
  }
  
}

function IncomingCallNotExist(){
    document.getElementById("incomingName").innerHTML = '<b>No Incoming Call</b>';
    document.getElementById("incomingNumber").innerHTML = '';
}

function UpdateCallHistory(){
    var totalTop = 0;
    var totalCallHistory = json.calls.length;

    if(totalCallHistory < 5){
        totalTop = totalCallHistory;
    }else{
        totalTop = 5;
    }

    //Update Top Call History
    var i = 0;
    while (i<totalTop){
        var str = CallToString(json.calls[i], true);

        var div = document.createElement("DIV");
        div.setAttribute("id","divtop");
        div.setAttribute("class","itemtop");
        div.innerHTML = str;

        divItemTop.appendChild(div);

        i++;
    }

    //Update Bottom Call History
    var row = 0;
    var y = 5;
    while(y<totalCallHistory){
        var newRow = tableHistoryBottom.insertRow(row);
        var newCell1 = newRow.insertCell(0);
        var newCell2 = newRow.insertCell(1);

        newCell1.innerHTML = CallToString(json.calls[y], false);
        newCell2.innerHTML = CallToString(json.calls[y+1], false);

        row++;
        y=y+2;
        if (row>4){
            break;
        }
    }
 
}

function CallToString(jsonCall, top){

    var time = jsonCall.time;
    var status = jsonCall.status;
    var statusColor = 'yellow';
    var from = jsonCall.from;
    var contact = jsonCall.contact;
    var to = jsonCall.to;

    if(status == 'Answered'){
        statusColor = 'blue';
    }else if (status == 'Rejected'){
        statusColor = 'red';
    }else if(status == 'Unanswered'){
        statusColor = 'yellow';
    }

    if(top){
        var str = '<font size="5">'+time+' <font color='+statusColor+'><b>'+status+'</b></font> ('+contact+') </font><div style="margin-left:65px"> <font size="5">'+from+' to '+to+'</font> </div>';
    }else{
        var str = '<div class="itembottom" id="itembottom"> <font size="4">'+time+' <font color='+statusColor+'><b>'+status+'</b></font> ('+contact+') </font> <div style="margin-left:50px"> <font size="3">'+from+' to '+to+'</font> </div></div>';
    }
    

    return str;
}
